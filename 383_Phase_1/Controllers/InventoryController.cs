﻿using System.Web.Mvc;
using System.Web.Http;
using _383_Phase_1.Models;
using _383_Phase_1.DAL;
using System.Linq;
using System.Data.Entity;

namespace _383_Phase_1.Controllers
{
    public class InventoryController : ApiController
    {
        PhaseOneDbContext db = new PhaseOneDbContext();

        // GET: Inventory
        [System.Web.Http.HttpGet]
        public IHttpActionResult GetAll()
        {
            var items = db.InventoryItems.ToList();
            var json_items = Newtonsoft.Json.JsonConvert.SerializeObject(items).ToString();
            return Ok(json_items);
        }

        // GET: Inventory/id
        [System.Web.Http.HttpGet]
        public IHttpActionResult Get(int id)
        {
            if (id > 0)
            {
                var item = db.InventoryItems.Where(u => u.Id == id).FirstOrDefault();
                var json_items = Newtonsoft.Json.JsonConvert.SerializeObject(item).ToString();
                return Ok(json_items);
            } else
            {
                var items = db.InventoryItems.ToList();
                var json_items = Newtonsoft.Json.JsonConvert.SerializeObject(items).ToString();
                return Ok(json_items);
            }
        }

        [System.Web.Http.HttpPost]
        public IHttpActionResult Buy(int id)
        {
            if (id > 0)
            {
                InventoryItem inventoryItem = db.InventoryItems.Find(id);
                inventoryItem.Quantity--;
                db.Entry(inventoryItem).State = EntityState.Modified;
                db.SaveChanges();
                return Ok("Item purchased successfully.");
            } else
            {
                return BadRequest();
            }
        }
    }
}