﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using _383_Phase_1.Models;
using _383_Phase_1.DAL;
using _383_Phase_1.Helpers;
using System.Web.Helpers;

namespace _383_Phase_1.Controllers
{
    [Authorize]
    public class UsersController : Controller
    {
        private PhaseOneDbContext db = new PhaseOneDbContext();

        // GET: Users
        public ActionResult Index()
        {
            return View(db.Users.ToList());
        }


        // GET: Users/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        public User GetByUsername(string Username)
        {
            if (string.IsNullOrEmpty(Username))
            {
                return null;
            }
            return db.Users.Where(x => x.Username == Username).FirstOrDefault();
        }

        // GET: Users/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Username,FirstName,LastName,Password")] User user)
        {
            if (ModelState.IsValid)
            {
                User db_user = db.Users.Where(u => u.Username == user.Username).FirstOrDefault();
                if (db_user != null)
                {
                    TempData["Notification"] = Helper.CreateFailureNotification("User already exists", "Try entering a different username, bro.");
                    return RedirectToAction("Create");
                } else
                {
                    user.Password = Crypto.HashPassword(user.Password);
                    db.Users.Add(user);
                    db.SaveChanges();
                    TempData["Notification"] = Helper.CreateSuccessNotification("User created", "User has been created.");
                    return RedirectToAction("Index");
                }
            }

            return View(user);
        }

        public User UserFromName(string name)
        {
            User temp_user = db.Users.Where(u => u.Username == name).FirstOrDefault();
            return temp_user;
        }

        // GET: Users/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Username,FirstName,LastName,Password")] User user)
        {
            if (ModelState.IsValid)
            {
                user.Password = Crypto.HashPassword(user.Password);
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(user);
        }

        // GET: Users/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            User user = db.Users.Find(id);
            db.Users.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
