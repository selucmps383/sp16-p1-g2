﻿using _383_Phase_1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.Helpers;
using _383_Phase_1.DAL;
using _383_Phase_1.Models;
using _383_Phase_1.Helpers;

namespace _383_Phase_1.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        PhaseOneDbContext db = new PhaseOneDbContext();

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(string username, string password)
        {
            if(!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
            {
                var userC = new UsersController();
                User user = userC.GetByUsername(username);
                if(user != null && Crypto.VerifyHashedPassword(user.Password, password))
                {
                    FormsAuthentication.SetAuthCookie(username, true);
                    //TempData["Notification"] = Helper.CreateSuccessNotification("Log in Success", "You've logged in successfully.");
                    return RedirectToAction("Index", "InventoryItems");
                }
                else
                {
                    TempData["Notification"] = Helper.CreateFailureNotification("Log in failure", "Username/password Error.");
                }
            } else
            {
                TempData["Notification"] = Helper.CreateFailureNotification("Log in failure", "Username/password Error.");
            }
            return View();
        }

        [HttpGet]
        public ActionResult Register(bool error = false)
        {
            if (error)
            {
                // can add / make visible error message on View
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register([Bind(Include = "Username,FirstName,LastName,Password")] User user)
        {
            if (ModelState.IsValid)
            {
                User db_user = db.Users.Where(u => u.Username == user.Username).FirstOrDefault();
                if (db_user != null)
                {
                    TempData["Notification"] = Helper.CreateFailureNotification("User already exists", "Try entering a different username, bro.");
                    return RedirectToAction("Register");
                }
                else
                {
                    user.Password = Crypto.HashPassword(user.Password);
                    db.Users.Add(user);
                    db.SaveChanges();
                    TempData["Notification"] = Helper.CreateSuccessNotification("User created", "User has been created.");
                    return RedirectToAction("Login");
                }
            }

            return Register();
        }


        [HttpGet]
        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Logout()
        {

            // TODO:  null out cookie for current user
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }


        //[HttpGet]
        //public ActionResult ResetPassword()
        //{
        //    // TODO:  Add a ResetPassword View and possibly use cookie to id User to modify

        //    return View();
        //}


        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult ResetPassword(string newPass)
        //{

        //    // TODO: replace this line below with getting the user from cookie and using that
        //    HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
        //    FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(authCookie.Value);
        //    string username = ticket.Name;

        //    var user = new User();

        //    user.Password = newPass;

        //    if (user == null)
        //    {
        //        // Don't reveal that the user does not exist
        //        return RedirectToAction(nameof(AccountController.ResetPassword), "Account");
        //    }

        //    return View();
        //}

    }
}