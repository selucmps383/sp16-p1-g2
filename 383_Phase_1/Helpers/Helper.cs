﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _383_Phase_1.Helpers
{
    public class Helper
    {
        public static string CreateSuccessNotification(string title, string message)
        {
            var notification = "<div class='alert alert-success'><strong>" + title + "</strong>" + "<br />" + message + "</div>";
            return notification;
        }

        public static string CreateFailureNotification(string title, string message)
        {
            var notification = "<div class='alert alert-danger'><strong>" + title + "</strong>" + "<br />" + message + "</div>";
            return notification;
        }
    }
}