﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _383_Phase_1.Models.Inventory
{
    public class Inventory
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }
    }
}