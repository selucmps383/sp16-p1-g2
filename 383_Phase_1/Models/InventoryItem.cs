﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace _383_Phase_1.Models
{
    [Table("InventoryItems")]
    public class InventoryItem
    {
        public int Id { get; set; }
        public int CreatedByUserId { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }

    }
}