﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using _383_Phase_1.DAL;
using _383_Phase_1.Models;

namespace _383_Phase_1.Hubs
{
    public class InventoryHub : Hub
    {
        private static List<InventoryItem> InventoryItems = new List<InventoryItem>();
        private static PhaseOneDbContext db = new PhaseOneDbContext();

        public static InventoryItem AddItemQuantity(int id)
        {
            // If the item is in InventoryItems, we’re tracking it, so just increment and return
            var inventoryItem = InventoryItems.Find(i => i.Id == id);
            if (inventoryItem != null)
            {
                inventoryItem.Quantity++;
                return inventoryItem;
            }
            // If the item wasn’t in InventoryItems, it’s the first time someone touched it. 
            // Add it to InventoryItems and increment
            else
            {
                var item = db.InventoryItems.Find(id);
                item.Quantity++;
                InventoryItems.Add(item);
                return item;
            }
        }

        public static InventoryItem SubtractItemQuantity(int id)
        {
            // If the item is in InventoryItems, we’re tracking it, so just decrement and return
            var inventoryItem = InventoryItems.Find(i => i.Id == id);
            if (inventoryItem != null)
            {
                inventoryItem.Quantity--;
                return inventoryItem;
            }
            // If the item wasn’t in InventoryItems, it’s the first time someone touched it. 
            // Add it to InventoryItems and decrement
            else
            {
                var item = db.InventoryItems.Find(id);
                item.Quantity--;
                InventoryItems.Add(item);
                return item;
            }
        }


        //public void CreateItem(int id)
        //{
        //}

        //public void DeleteItem(int id)
        //{
        //}

    }
}