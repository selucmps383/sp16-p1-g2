namespace _383_Phase_1.Migrations
{
    using Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Web.Helpers;
    internal sealed class Configuration : DbMigrationsConfiguration<_383_Phase_1.DAL.PhaseOneDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(_383_Phase_1.DAL.PhaseOneDbContext context)
        {
            var Users = new List<User>
            {

                new User
                {
                    Username = "admin",
                    Password = Crypto.HashPassword("selu2014"),
                    FirstName = "SELU",
                    LastName = "Admin",
                    InventoryItems = new List<InventoryItem>(),
                }
            };

            Users.ForEach(s => context.Users.Add(s));
            context.SaveChanges();

            User ProductCreator = context.Users.FirstOrDefault(u => u.FirstName == "Jean");

            var InventoryItems = new List<InventoryItem>
            {
                new InventoryItem{
                    Name = "Guitar Hero - V",
                    Quantity = 5,
                    CreatedByUserId = ProductCreator.Id
                },
                new InventoryItem{
                    Name = "COD V",
                    Quantity = 15,
                    CreatedByUserId = ProductCreator.Id
                },
                new InventoryItem{
                    Name = "E.T. the Extra-Terrestrial",
                    Quantity = 10000,
                    CreatedByUserId = ProductCreator.Id
                }
            };

            InventoryItems.ForEach(s => context.InventoryItems.Add(s));
            context.SaveChanges();
        }
    }
}
