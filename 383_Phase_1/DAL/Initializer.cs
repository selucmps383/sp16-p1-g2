﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using _383_Phase_1.Models;
using System.Web.Helpers;

namespace _383_Phase_1.DAL
{
    public class SchoolInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<PhaseOneDbContext>
    {
        protected override void Seed(PhaseOneDbContext context)
        {
            var Users = new List<User>
            {
                new User
                {
                    Username = "admin-test",
                    Password = Crypto.HashPassword("testtest"),
                    FirstName = "Kate",
                    LastName = "Moss",
                    InventoryItems = new List<InventoryItem>(),
                    
                },
                new User
                {
                    Username = "admin2",
                    Password = Crypto.HashPassword("testtest"),
                    FirstName = "Jean",
                    LastName = "Francois",
                    InventoryItems = new List<InventoryItem>()
                }
            };

            Users.ForEach(s => context.Users.Add(s));
            context.SaveChanges();

            User ProductCreator = context.Users.SingleOrDefault(u => u.FirstName == "Jean");

            var InventoryItems = new List<InventoryItem>
            {
                new InventoryItem{
                    Name = "Guitar Hero - V",
                    Quantity = 5,
                    CreatedByUserId = ProductCreator.Id
                },
                new InventoryItem{
                    Name = "COD V",
                    Quantity = 15,
                    CreatedByUserId = ProductCreator.Id
                }
            };

            InventoryItems.ForEach(s => context.InventoryItems.Add(s));
            context.SaveChanges();
        }
    }
}