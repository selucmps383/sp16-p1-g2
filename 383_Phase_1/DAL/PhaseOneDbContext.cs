﻿using System.Data.Entity;
using _383_Phase_1.Models;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace _383_Phase_1.DAL
{
    public class PhaseOneDbContext : DbContext
    {
        public PhaseOneDbContext() : base("PhaseOneDbContext")
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<InventoryItem> InventoryItems { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}