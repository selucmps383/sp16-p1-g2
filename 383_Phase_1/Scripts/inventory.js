﻿(function () {

    var InventoryHub = $.connection.InventoryHub;
    $.connection.hub.logging = true;
    $.connection.hub.start();

    InventoryHub.client.AddItemQuantity = function (id) {
        // ...
    };

    InventoryHub.client.SubtractItemQuantity = function (id) {
        // ...
    };

    var Model = function () {
        var self = this;
        self.id = ko.observable(Intl)
    };

    Model.prototype = {

        AddItemQuantity: function () {
            var self = this;
            InventoryHub.server.AddItemQuantity(self.id());
            self.id = ko.observable(Intl)
        },

        SubtractItemQuantity: function () {
            var self = this;
            InventoryHub.server.SubtractItemQuantity(self.id());
            self.id = ko.observable(Intl)
        }

    };

}());