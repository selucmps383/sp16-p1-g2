﻿

var InventoryUpdate;
(function () {

    InventoryUpdate = $.connection.inventoryUpdate;

    $.connection.hub.logging = true;
    
    $.connection.hub.start();
    

    InventoryUpdate.client.newItem = function(Item) {
        Items.push(new Inventory(Item.ID, Item.Name, Item.Quantity, Item.CreatedByUserId))
    };

}());




var Inventory = function (Id, Name, Quantity, CreatedBy) {
    this.Id = ko.observable(Id);
    this.Name = ko.observable(Name);
    this.Quantity = ko.observable(Quantity);
    this.CreatedBy = ko.observable(CreatedBy);
}

Inventory.prototype = {
    sendItem: function() {
        InventoryUpdate.server.sendItem(JSON.stringify(this))
    }
}
